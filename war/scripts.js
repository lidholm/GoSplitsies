
var splitsiesApp = angular.module('splitsiesApp', []);

splitsiesApp.controller('SplitsiesCtrl', function ($scope, $http) {
	
  $http.get('http://localhost:8888/_ah/api/personendpoint/v1/person/maricio.pochettino').success(function(data) {
      $scope.persons = data['items'];
      //alert(data['userId']);
      $scope.userId = data['userId'];
    });
  $http.get('http://localhost:8888/_ah/api/itemendpoint/v1/item/maricio.pochettino').success(function(data) {
      $scope.items = data['items'];
      angular.forEach($scope.items, function(item, key) {
		    item['open'] = false;
		  }, null);

    });
  
  $scope.getNameOfPerson = function(id) {
	  var name = "";
	  angular.forEach($scope.persons, function(person, key) {
		    if (person['id'] == id) {
		    	name = person['name'];
		    }
		  }, null);
	  return name;
  };
  
  $scope.toggleOpen = function(item) {
	  if (item['open']) {
		  item['open'] = false;
	  } else {
		  item['open'] = true;
	  }
  };

  $scope.newItemPrice = 0;
  $scope.newItemPurchaser = "20001";
  $scope.newItemSharer = "20002";
  
  $scope.addItem = function() {
	  var userId = "maricio.pochettino";
	  var price = $scope.newItemPrice;
	  var purchasers = [];
	  purchasers.push($scope.newItemPurchaser['id']);
	  var sharers = [];
	  sharers.push($scope.newItemSharer['id']);

	  var item = $scope.getItemString(userId, price, sharers, purchasers);
	  alert(item);
	  
	  var req = {
	    method: 'POST',
	    url: 'http://localhost:8888/_ah/api/itemendpoint/v1/item/maricio.pochettino',
	    headers: {
	      'Content-Type': 'application/json',
	    },
	    data: item ,
	  }

	  $http(req)
	    .success(function(data) {
	      alert("success" + data);
	    })
	    .error(function(data, status, headers, config) {
	    	alert("error\n" + status + "\n" + data + "\n" + headers  + "\n" + config );
	    });

  };
  

  $scope.getItemString = function(userId, price, sharers, purchasers) {
      var item = '{';
      if (sharers.length > 0) {
          item += '  sharers: [';
          angular.forEach(sharers, function(aSharer) {
        	  item += '"' + aSharer + '",';
            });
          item = item.substring(0, item.length - 1);
          item += '],';
      }
      if (purchasers.length > 0) {
          item += 'purchasers : [';
          angular.forEach(purchasers, function(aPurchaser) {
        	  item += '"' + aPurchaser + '",';
            });
          item = item.substring(0, item.length - 1);
          item += '],';
      }
      
      item += 'userId: "' + userId +'",';
      item += 'price: ' + price + '}';
          
      return item;
  }
  
});

