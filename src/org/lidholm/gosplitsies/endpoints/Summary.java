package org.lidholm.gosplitsies.endpoints;

import java.util.ArrayList;
import java.util.List;

public class Summary {
	List<PersonSummary> personSummaries = new ArrayList<PersonSummary>();

	public Summary() {

	}

	public List<PersonSummary> getPersonSummaries() {
		return personSummaries;
	}

	public void setPersonSummaries(List<PersonSummary> personSummaries) {
		this.personSummaries = personSummaries;
	}

	public void addPersonSummary(PersonSummary personSummary) {
		personSummaries.add(personSummary);
	}
}
