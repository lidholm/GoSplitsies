package org.lidholm.gosplitsies.endpoints;

import java.util.List;

public class PersonSummary {
	private int id;
	private List<Item> purchasedItems;
	private List<Item> contributeToItems;
	private float purchaseAmount;
	private float sharingAmount;
	private float totalAmount;

	public PersonSummary() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getTotal() {
		return 0;
	}

	public float getTotalIncoming() {
		return (float) 2.0;
	}

	public float getTotalOutcoming() {
		return (float) 3.0;
	}

	public List<Item> getPurchasedItems() {
		return purchasedItems;
	}

	public void setPurchasedItems(List<Item> purchasedItems) {
		this.purchasedItems = purchasedItems;
	}

	public List<Item> getContributeToItems() {
		return contributeToItems;
	}

	public void setContributeToItems(List<Item> contributeToItems) {
		this.contributeToItems = contributeToItems;
	}

	public float getPurchaseAmount() {
		return purchaseAmount;
	}

	public void setPurchaseAmount(float purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	public float getSharingAmount() {
		return sharingAmount;
	}

	public void setSharingAmount(float sharingAmount) {
		this.sharingAmount = sharingAmount;
	}

	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

}