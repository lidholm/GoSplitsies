package org.lidholm.gosplitsies.endpoints;

import java.util.ArrayList;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Item {
	// TODO: Try and remove the primarykey, it seems to already have another type of key in the database
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long id;
	@Persistent
	private double price;
	@Persistent
	private ArrayList<Long> purchasers;
	@Persistent
	private ArrayList<Long> sharers;
	@Persistent
	private String userId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public ArrayList<Long> getPurchasers() {
		return purchasers;
	}

	public void setPurchasers(ArrayList<Long> purchasers) {
		this.purchasers = purchasers;
	}

	public ArrayList<Long> getSharers() {
		return sharers;
	}

	public void setSharers(ArrayList<Long> sharers) {
		this.sharers = sharers;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}