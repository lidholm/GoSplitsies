package org.lidholm.gosplitsies.endpoints;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.appengine.api.datastore.Cursor;

@Api(name = "purchasesendpoint", namespace = @ApiNamespace(ownerDomain = "lidholm.org", ownerName = "lidholm.org", packagePath = "gosplitsies.endpoints"))
public class PurchasesEndpoint {

	@SuppressWarnings("unchecked")
	@ApiMethod(name = "listPurchases")
	public Summary listPurchases(@Named("user") String user) {
		List<Person> persons = getPersons(user);
		Summary summary = new Summary();

		for (Person person : persons) {
			PersonSummary personSummary = listPurchases(user, person.getId().toString());
			summary.addPersonSummary(personSummary);
		}
		return summary;
	}

	private List<Person> getPersons(String user) {
		// TODO: Refactor, move this to a helper method
		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Person> execute = null;
		List<Person> persons = new ArrayList<Person>();

		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(Person.class);
			query.setFilter("userId == '" + user + "'");
			execute = (List<Person>) query.execute();

			// Tight loop for fetching all entities from datastore and accomodate
			// for lazy fetch.
			for (Person person : execute) {
				persons.add(person);
			}
		} finally {
			mgr.close();
		}
		return persons;
	}

	private PersonSummary listPurchases(String user, String personIdString) {
		int personId = Integer.parseInt(personIdString);

		PersistenceManager mgr = null;

		List<Item> purchasedItems = new ArrayList<Item>();
		List<Item> sharingItems = new ArrayList<Item>();

		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(Item.class);
			query.setFilter("userId == '" + user + "'");

			List<Item> tmp = (List<Item>) query.execute();

			for (Item item : tmp) {
				if (item.getPurchasers() != null) {
					for (Long purchaser : item.getPurchasers()) {
						if (purchaser == personId) {
							purchasedItems.add(item);
						}
					}
				}
				if (item.getSharers() != null) {
					for (Long sharer : item.getSharers()) {
						if (sharer == personId) {
							sharingItems.add(item);
						}
					}
				}
			}
		} finally {
			mgr.close();
		}

		PersonSummary summary = CreatePersonSummary(personId, purchasedItems, sharingItems);

		return summary;
	}

	private PersonSummary CreatePersonSummary(int personId, List<Item> purchasedItems, List<Item> sharingItems) {
		PersonSummary personSummary = new PersonSummary();
		personSummary.setId(personId);
		personSummary.setPurchasedItems(purchasedItems);
		personSummary.setContributeToItems(sharingItems);

		float purchaseAmount = getPurchaseAmount(purchasedItems);
		float sharingAmount = getSharingAmount(sharingItems);

		personSummary.setPurchaseAmount(purchaseAmount);
		personSummary.setSharingAmount(sharingAmount);

		personSummary.setTotalAmount(purchaseAmount - sharingAmount);

		return personSummary;
	}

	private float getPurchaseAmount(List<Item> purchasedItems) {
		float total = 0;
		for (Item item : purchasedItems) {
			total += item.getPrice() / item.getPurchasers().size();
		}
		return total;
	}

	private float getSharingAmount(List<Item> sharingItems) {
		float total = 0;
		for (Item item : sharingItems) {
			total += item.getPrice() / item.getSharers().size();
		}
		return total;
	}

	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}

}
