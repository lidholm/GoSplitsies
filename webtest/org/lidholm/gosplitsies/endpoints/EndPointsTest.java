package org.lidholm.gosplitsies.endpoints;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Assert;
import org.junit.Test;

public class EndPointsTest {
	private String server = "http://localhost";
	private String port = "8888";

	@Test
	public void CreateObject() {
		try {
			createPerson("11", "Donald Duck", "donald.duck@duckburg.com", "walt.disney");
			Thread.sleep(2500);
			createPerson("13", "Hugo Lloris", "hugo.lloris@spurs.co.uk", "maricio.pochettino");
			Thread.sleep(2500);
			createPerson("14", "Harry Kane", "harry.kane@spurs.co.uk", "maricio.pochettino");
			Thread.sleep(2500);
			createPerson("12", "Mickey Mouse", "mickey.mouse@duckburg.com", "walt.disney");
			Thread.sleep(2500);
			String listOfPerson = getListOfPerson("walt.disney");

			String expected = "{  \"items\" : "
					+ "[ {    \"id\" : \"11\",    \"name\" : \"Donald Duck\",    \"email\" : \"donald.duck@duckburg.com\",    \"userId\" : \"walt.disney\"  },"
					+ " {    \"id\" : \"12\",    \"name\" : \"Mickey Mouse\",    \"email\" : \"mickey.mouse@duckburg.com\",    \"userId\" : \"walt.disney\"  } ]}";

			Assert.assertEquals(expected, listOfPerson);
			Thread.sleep(2500);

			listOfPerson = getListOfPerson("maricio.pochettino");

			expected = "{  \"items\" : "
					+ "[ {    \"id\" : \"11\",    \"name\" : \"Donald Duck\",    \"email\" : \"donald.duck@duckburg.com\",    \"userId\" : \"walt.disney\"  },"
					+ " {    \"id\" : \"12\",    \"name\" : \"Mickey Mouse\",    \"email\" : \"mickey.mouse@duckburg.com\",    \"userId\" : \"walt.disney\"  } ]}";

			Assert.assertEquals(expected, listOfPerson);
			Thread.sleep(2500);

			deletePerson("walt.disney", "11");
			Thread.sleep(2500);
			deletePerson("walt.disney", "12");
			Thread.sleep(2500);
			deletePerson("maricio.pochettino", "13");
			Thread.sleep(2500);
			deletePerson("maricio.pochettino", "14");

		} catch (InterruptedException e) {
			Assert.fail(e.getMessage());
		}
	}

	private void createPerson(String id, String name, String email, String userId) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		nameValuePairs.add(new BasicNameValuePair("id", id));
		nameValuePairs.add(new BasicNameValuePair("name", name));
		nameValuePairs.add(new BasicNameValuePair("email", email));
		nameValuePairs.add(new BasicNameValuePair("userId", userId));

		String url = server + ":" + port + "/_ah/api/personendpoint/v1/person/" + userId;
		String actual = httpPost(url, nameValuePairs);
		String expected = createPersonExpectedString(id, name, email, userId);
		Assert.assertEquals(expected, actual);
	}

	private String getListOfPerson(String userId) {
		String url = server + ":" + port + "/_ah/api/personendpoint/v1/person/" + userId;
		String actual = httpGet(url);
		return actual;
	}

	private void deletePerson(String userId, String id) {
		String url = server + ":" + port + "/_ah/api/personendpoint/v1/person/" + userId + "/" + id;
		httpDelete(url);
	}

	private String createPersonExpectedString(String id, String name, String email, String userId) {
		String expected = "{  \"id\" : \"" + id + "\",  \"name\" : \"" + name + "\",  \"email\" : \"" + email + "\",  \"userId\" : \""
				+ userId + "\"}";
		return expected;
	}

	private String httpPost(String url, List<NameValuePair> pairs) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);
		try {
			post.setEntity(new UrlEncodedFormEntity(pairs));

			HttpResponse response = client.execute(post);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuilder sb = new StringBuilder();
			String line = "";
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			return sb.toString();

		} catch (IOException e) {
			e.printStackTrace();
			return "Exception " + e;
		}
	}

	private String httpGet(String url) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(url);
		try {
			HttpResponse response = client.execute(get);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuilder sb = new StringBuilder();
			String line = "";
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			return sb.toString();

		} catch (IOException e) {
			e.printStackTrace();
			return "Exception " + e;
		}
	}

	private void httpDelete(String url) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpDelete httpDelete = new HttpDelete(url);
		try {
			client.execute(httpDelete);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
