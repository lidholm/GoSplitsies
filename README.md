GoSplitsies
================================

This application is trying to solve the problem 
of multiple persons having paid for some items
each and are now trying to solve who owes who.
Implemented using Google Cloud Endpoints, App Engine, and Java.

## Products
- [App Engine][1]

## APIs
- [Google Cloud Endpoints][2]

## Links
* [Web client code][3]
* [Google Cloud Endpoints code][4]
* [Mobile app with with app engine backend][13]
* [Trello][8]
* [BitBucket version][9]
* [NinjaMock][10]

## Model classes
* Person
  * Name - string
  * E-mail - string
  
* Item
  * price - float
  * purchasers - ArrayList - Person 
  * sharers - ArrayList - Person

### To be implemented later on
* Group
  * title - stringva
  * persons - ArrayList - Person
  * items - ArrayList - Item
  * uuid - UUID  //for sharing

* Part
  * person - Person 
  * amount - float

* AdvancedItem
  * price - float 
  * purchasers - ArrayList - Part
  * sharers - ArrayList - Part

## REST API Functions

* Get list of persons
  * A list of purchases that the person has made (and who will be sharing the cost of each)
  * A list of purchases that the person should share the cost of (and who made the purchase and who else will share the cost)
  
   


## Old Setup Instructions
1. Import the project into Eclipse.
1. Make sure the App Engine SDK jars are added to the `war/WEB-INF/lib`
   directory, either by adding them by hand, or having Eclipse do it. (An easy)
   way to do this in Eclipse is to unset and reset whether or not the project
   uses Google App Engine.
1. Update the value of `application` in `appengine-web.xml` to the app ID you
   have registered in the App Engine admin console and would like to use to host
   your instance of this sample.
1. Update the values in `src/com/google/devrel/samples/ttt/spi/Ids.java` to
   reflect the respective client IDs you have registered in the
   [APIs Console][4].
1. Update the value of `google.devrel.samples.ttt.CLIENT_ID` in
   [`war/js/render.js`][6] to reflect the web client ID you have registered in the
   [APIs Console][4].
1. Run the application, and ensure it's running by visiting your local server's
   address (by default [localhost:8888][5].)
1. Deploy your application.


[1]: https://developers.google.com/appengine
[2]: https://developers.google.com/appengine/docs/java/endpoints/
[3]: https://cloud.google.com/appengine/docs/java/endpoints/getstarted/clients/js/client_ui
[4]: https://cloud.google.com/appengine/docs/java/endpoints/getstarted/backend/hello_world
[5]: https://code.google.com/apis/console
[6]: https://localhost:8888/
[7]: https://github.com/GoogleCloudPlatform/appengine-endpoints-tictactoe-java/blob/master/war/js/render.js
[8]: https://trello.com/b/5Xqs5C2M/gosplitsies
[9]: https://bitbucket.org/lidholm/gosplitsies/wiki/Home
[10]: http://ninjamock.com/Designer/NewWorkplace/WebProject/napkin/1318258/Page1
[11]: http://ninjamock.com/Designer/Workplace/1388831/CopyofPage1
[12]: http://ninjamock.com/Designer/NewWorkplace/WebProject/napkin/1318258/Page1
[13] https://cloud.google.com/developers/articles/how-to-build-mobile-app-with-app-engine-backend-tutorial/
