
import sys
import urllib2
import json
from time import sleep

LIVE_DOMAIN = "https://intrepid-tape-754.appspot.com/"
LOCALHOST_DOMAIN = "http://localhost:8888"
DOMAIN = LIVE_DOMAIN
#DOMAIN = LOCALHOST_DOMAIN #

def main():
    args = sys.argv[1:]
    api = RestApiHandler()
    
    if args[0] == "deletePerson":
        api.deletePerson(args[1], args[2])
    elif args[0] == "deleteItem":
        api.deleteItem(args[1], args[2])
    elif args[0] == "deleteAll":
        api.deleteAll(args[1])

    elif args[0] == "addPerson":
        api.addPerson(args[1], args[2], args[3], args[4],)
    elif args[0] == "addItem":
        api.addItem(args[1], args[2], args[3], args[4], args[5])
        
    elif args[0] == "listPersons":
        persons = api.listPersons(args[1])
        if persons is not None:
            print "\n".join(map(str,persons))
        else:
            print "None"
    elif args[0] == "listItems":
        items = api.listItems(args[1])
        print items
        
    else:
        print "WARNING!"
        print "No such option"
        
        
class RestApiHandler():
    def getUrl(self, path):
        return DOMAIN + path
    
    def deleteAll(self, userId):
        self.deleteAllItems(userId)
        self.deleteAllPersons(userId)
        
    def deleteAllItems(self, userId):
        items = self.listItems(userId)
        if items == None:
            return
        itemIds = [item['id'] for item in items]
        for itemId in itemIds:
            self.deleteItem(userId, itemId)
    
    def deleteItem(self, userId, itemId):
        url = self.getUrl("/_ah/api/itemendpoint/v1/item/" + userId + "/" + itemId)
        
        headers = {}
        opener = urllib2.build_opener(urllib2.HTTPHandler)
        req = urllib2.Request(url, None, headers)
        req.get_method = lambda: 'DELETE'
        url = urllib2.urlopen(req)

    def deleteAllPersons(self, userId):
        persons = self.listPersons(userId)
        if persons == None:
            return
        personIds = [person['id'] for person in persons]
        for personId in personIds:
            self.deletePerson(userId, personId)

    
    def deletePerson(self, userId, personId):
        url = self.getUrl("/_ah/api/personendpoint/v1/person/" + userId + "/" + personId)
        headers = {}
        opener = urllib2.build_opener(urllib2.HTTPHandler)
        req = urllib2.Request(url, None, headers)
        req.get_method = lambda: 'DELETE'
        url = urllib2.urlopen(req)
    
    def addPerson(self, userId, personId, name, email):

        url = self.getUrl("/_ah/api/personendpoint/v1/person/" + userId)

        item = self.getPersonString(userId, personId, name, email)
        opener = urllib2.build_opener(urllib2.HTTPHandler)
        request = urllib2.Request(url, data=item)
        request.add_header('Content-Type', 'application/json')
        request.get_method = lambda: 'POST'
        url = opener.open(request)

    def getPersonString(self, userId, personId, name, email):
        item = '{\n  "id": "' + personId + '",'
        item += '\n  "userId": "' + userId +'",'
        item += '\n  "name": "' + name +'",'
        item += '\n  "email": "' + email + '"\n}'
            
        return item
    
    def addItem(self, userId, itemId, price, purchasers, sharers):
        url = self.getUrl("/_ah/api/itemendpoint/v1/item/" + userId)
        item = self.getItemString(userId, itemId, price, sharers, purchasers)

        opener = urllib2.build_opener(urllib2.HTTPHandler)
        request = urllib2.Request(url, data=item)
        request.add_header('Content-Type', 'application/json')
        request.get_method = lambda: 'POST'
        url = opener.open(request)
    
    def getItemString(self, userId, itemId, price, sharers, purchasers):
        item = '{\n  "id": "' + itemId + '",'
        if len(sharers) > 0:
            item += '\n  "sharers" : ['
            for sharer in sharers.split(","):
                item += '\n    "' + sharer + '",'
            item = item[:-1]
            item += '\n  ],'
        if len(purchasers) > 0:
            item += '\n  "purchasers" : ['
            for purchaser in purchasers.split(","):
                item += '\n    "' + purchaser + '",'
            item = item[:-1]
            item += '\n  ],'

        
        item += '\n  "userId": "' + userId +'",'
        item += '\n  "price": "' + price + '"\n}'
            
        return item
         
    def listPersons(self, userId):
        url =  self.getUrl("/_ah/api/personendpoint/v1/person/" + userId)

        response = urllib2.urlopen(url)
        html = response.read()
        data = json.loads(html)
        if data != {} and data.has_key("items"):
            return data["items"]
        return None
        
    def listItems(self, userId):
        url = self.getUrl("/_ah/api/itemendpoint/v1/item/" + userId)

        response = urllib2.urlopen(url)
        html = response.read()
        data = json.loads(html)
        if data != {} and data.has_key("items"):
            return data["items"]
        return None
                
            
    def listPurchases(self, userId):
        url = self.getUrl("/_ah/api/purchasesendpoint/v1/listPurchases/" + userId)

        response = urllib2.urlopen(url)
        html = response.read()
        data = json.loads(html)
        return data
                
if __name__ == "__main__":
    main()
    
    
    