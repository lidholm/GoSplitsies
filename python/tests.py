import unittest
import cli
from urllib2 import HTTPError
import json



class Tester(unittest.TestCase):
    def setUp(self):
        self.api = cli.RestApiHandler()

    def testHappyPath(self):
        nickFury = "nick.fury"
        maricioPochettino = "maricio.pochettino"
        
        inputPersons = {
            nickFury : [
                (nickFury, "10001", "Iron man", "iron.man@avengers.com"),
                (nickFury, "10002", "Black widow", "black.widow@avengers.com"),
                (nickFury, "10003", "Captain America", "captain.america@avengers.com"),
                (nickFury, "10004", "The hulk", "the.hulk@avengers.com"),
                (nickFury, "10005", "Thor", "thor@avengers.com"),
                (nickFury, "10006", "Hawkeye", "hawkeye@avengers.com"),
            ],
            maricioPochettino : [
                (maricioPochettino, "20001", "Harry Kane", "harry.kane@spurs.co.uk"),
                (maricioPochettino, "20002", "Hugo Lloris", "hugo.lloris@spurs.co.uk"),
                (maricioPochettino, "20003", "Jan Vertonghen", "jan.vertonghen@spurs.co.uk"),
            ]
        }
        
        for userId in inputPersons.keys():
            self.api.deleteAll(userId)
            for person in inputPersons[userId]:
                self.api.addPerson(*person)
            
        inputItems = {
            nickFury : [
                (nickFury, "100001", "246.86", "10001",       "10002,10003" ),
                (nickFury, "100002",     "90", "10002,10003", "10001,10003" ),
                (nickFury, "100004",    "100", "10002",       "10004,10005" ),
            ],
            maricioPochettino : [
                (maricioPochettino, "200001", "140.00", "20001",       "20001,20003" ),
                (maricioPochettino, "200002",     "90", "20003", "20002,20003" ),
                (maricioPochettino, "200003",    "100", "20002",       "20003,20002" ),
            ],    
        }
        
        #Act
        for userId in inputItems:
            for item in inputItems[userId]:
                self.api.addItem(*item)


        #Assert
        for userId in inputPersons.keys():
            i=0
            persons = self.api.listPersons(userId)
            for person in persons:
                self.assertEqual(person['userId'],   inputPersons[userId][i][0])
                self.assertEqual(person['id'],       inputPersons[userId][i][1])
                self.assertEqual(person['name'],     inputPersons[userId][i][2])
                self.assertEqual(person['email'],    inputPersons[userId][i][3])
                i += 1

        for userId in inputItems.keys():
            i=0
            items = self.api.listItems(userId)

            if items is None:
                continue
            for item in items:
                self.assertEqual(item['userId'],     inputItems[userId][i][0])
                self.assertEqual(item['id'],         inputItems[userId][i][1])
                self.assertEqual(item['price'],      float(inputItems[userId][i][2]))
                self.assertEqual(','.join(map(str,item['purchasers'])), inputItems[userId][i][3])
                self.assertEqual(','.join(map(str,item['sharers'])),    inputItems[userId][i][4])
                
                i += 1
                      
    def testAddAlreadyExistingPerson(self):
        userId = "nick.fury"
        
        inputPersons = {
            userId : [
                (userId, "10001", "Iron man", "iron.man@avengers.com"),
                (userId, "10001", "Black widow", "black.widow@avengers.com"),
            ],
        }
        
        self.api.deleteAll(userId)
        self.api.addPerson(*inputPersons[userId][0])
        try:
            self.api.addPerson(*inputPersons[userId][1])
            self.assertTrue(False, "Allowed to add a person with an id that is already added")
        except HTTPError, e:
            data = json.loads(e.read())
            if data['error']['message'] == "javax.persistence.EntityExistsException: Object already exists":
                self.assertTrue(True)
            else:
                self.assertTrue(False, "Not the error page that was expected")
                
    def testAddAlreadyExistingItem(self):
        userId = "nick.fury"
        
        inputItems = {
            userId : [
                (userId, "100001", "246.86", "", "" ),
                (userId, "100001",     "90", "", "" ),
            ]}
        
        self.api.deleteAll(userId)
        self.api.addItem(*inputItems[userId][0])
        try:
            self.api.addItem(*inputItems[userId][1])
            self.assertTrue(False, "Allowed to add an item with an id that is already added")
        except HTTPError, e:
            data = json.loads(e.read())
            if data['error']['message'] == "javax.persistence.EntityExistsException: Object already exists":
                self.assertTrue(True)
            else:
                self.assertTrue(False, "Not the error page that was expected")
                
    
    def testAddItemWithNotExistingPersonAsSharerAndPurchaser(self):
        userId = "nick.fury"
        
        inputItems = {
            userId : [
                (userId, "100001", "246.86", "1001", "" ),
                (userId, "100002",     "90", "", "1001" ),
            ]}
        
        self.api.deleteAll(userId)
        try:
            self.api.addItem(*inputItems[userId][0])
            self.assertTrue(False, "Allowed to add an item with a non existing purchaser")
        except HTTPError, e:
            data = json.loads(e.read())
            if data['error']['message'] == "javax.persistence.EntityNotFoundException: Some person does not exist for the current user":
                self.assertTrue(True)
            else:
                print data['error']['message']
                self.assertTrue(False, "Not the error page that was expected")
                
        try:
            self.api.addItem(*inputItems[userId][1])
            self.assertTrue(False, "Allowed to add an item with a non existing sharer")
        except HTTPError, e:
            data = json.loads(e.read())
            if data['error']['message'] == "javax.persistence.EntityNotFoundException: Some person does not exist for the current user":
                self.assertTrue(True)
            else:
                print data['error']['message']
                self.assertTrue(False, "Not the error page that was expected")
                
          
    def testAddItemWithSharerAndPurchaserThatExistsForAnotherUser(self):
        nickFury = "nick.fury"
        maricioPochettino = "maricio.pochettino"
        
        inputPersons = {
            maricioPochettino : [
                (maricioPochettino, "20001", "Harry Kane", "harry.kane@spurs.co.uk"),
            ]
        }
        
        inputItems = {
            nickFury : [
                (nickFury, "100001", "246.86", "20001", "" ),
                (nickFury, "100002",     "90", "", "20001" ),
            ]}
        
        self.api.deleteAll(nickFury)
        self.api.deleteAll(maricioPochettino)
        
        for userId in inputPersons.keys():
            for person in inputPersons[userId]:
                self.api.addPerson(*person)
        
        try:
            self.api.addItem(*inputItems[nickFury][0])
            self.assertTrue(False, "Allowed to add an item with a non existing purchaser")
        except HTTPError, e:
            data = json.loads(e.read())
            if data['error']['message'] == "javax.persistence.EntityNotFoundException: Some person does not exist for the current user":
                self.assertTrue(True)
            else:
                print data['error']['message']
                self.assertTrue(False, "Not the error page that was expected")
                
        try:
            self.api.addItem(*inputItems[nickFury][1])
            self.assertTrue(False, "Allowed to add an item with a non existing sharer")
        except HTTPError, e:
            data = json.loads(e.read())
            if data['error']['message'] == "javax.persistence.EntityNotFoundException: Some person does not exist for the current user":
                self.assertTrue(True)
            else:
                print data['error']['message']
                self.assertTrue(False, "Not the error page that was expected")
          
    def testPurchaseSummary(self):
        maricioPochettino = "maricio.pochettino"
        
        inputPersons = {
            maricioPochettino : [
                (maricioPochettino, "20001", "Harry Kane", "harry.kane@spurs.co.uk"),
                (maricioPochettino, "20002", "Hugo Lloris", "hugo.lloris@spurs.co.uk"),
                (maricioPochettino, "20003", "Jan Vertonghen", "jan.vertonghen@spurs.co.uk"),
                (maricioPochettino, "20004", "Nabil Bentaleb", "nabil.bentaleb@spurs.co.uk"),
                (maricioPochettino, "20005", "Christian Eriksen", "christian.eriksen@spurs.co.uk"),
                (maricioPochettino, "20006", "Danny Rose", "danny.rose@spurs.co.uk"),
            ]
        }
        
        inputItems = {
            maricioPochettino : [
                (maricioPochettino, "200001", "150", "20001",       "20001,20002,20003" ),
                (maricioPochettino, "200002",  "90", "20002",       "20001,20002" ),
                (maricioPochettino, "200003", "100", "20001",       "20001,20003" ),
                (maricioPochettino, "200004",  "25", "20003",       "20001,20002" ),
                (maricioPochettino, "200005",  "70", "20004,20003", "20001" ),
                (maricioPochettino, "200006",  "33", "20005",       "20005" ),
            ]}
        
        self.api.deleteAll(maricioPochettino)
        
        for userId in inputPersons.keys():
            for person in inputPersons[userId]:
                self.api.addPerson(*person)
        
        for userId in inputItems:
            for inputItem in inputItems[userId]:
                self.api.addItem(*inputItem)
                
        summary = self.api.listPurchases(maricioPochettino)
        personSummaries = summary['personSummaries']
        
        keys = ['id', 'purchaseAmount', 'sharingAmount', 'totalAmount',]# 'contributeToItems', 'purchasedItems' ]
        expectedValues = [
            (20001, 250.0, 227.5,  22.5),
            (20002,  90.0, 107.5, -17.5),
            (20003,  60.0, 100.0, -40.0),
            (20004,  35.0,   0.0,  35.0),
            (20005,  33.0,  33.0,   0.0),
            (20006,   0.0,   0.0,   0.0),
        ]
        for i, personSummary in enumerate(personSummaries):
            for j, key in enumerate(keys):
                expectedValue = expectedValues[i][j]
                actualValue = personSummary[key]
                self.assertEqual(expectedValue, actualValue)
        
